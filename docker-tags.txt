# tags (before "\t") and build arguments (after "\t", separated by " " which cannot be utilised otherwise) of the Docker image variants to build
8/jdk/alpine/Dockerfile.hotspot.releases.slim:jdk-8u-alpine-3.10-slime	ALPINE_VERSION=3.10
8/jdk/alpine/Dockerfile.hotspot.releases.slim:jdk-8u-alpine-3.11-slime	ALPINE_VERSION=3.11
8/jdk/alpine/Dockerfile.hotspot.releases.slim:jdk-8u-alpine-3.12-slime	ALPINE_VERSION=3.12
11/jdk/alpine/Dockerfile.hotspot.releases.slim:jdk-11u-alpine-3.10-slime	ALPINE_VERSION=3.10
11/jdk/alpine/Dockerfile.hotspot.releases.slim:jdk-11u-alpine-3.11-slime	ALPINE_VERSION=3.11
11/jdk/alpine/Dockerfile.hotspot.releases.slim:jdk-11u-alpine-3.12-slime	ALPINE_VERSION=3.12
